FROM php:7.2-apache

COPY src/ /var/www/html

RUN apt-get update
    
EXPOSE 80
